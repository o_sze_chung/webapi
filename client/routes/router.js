var express = require('express');
var router = express.Router();
var path = require('path')
var bodyParser = require('body-parser');


router.use(bodyParser.urlencoded({ extended: true }));
//GET route for homepage
router.get('/', function (req, res, next) {
  return res.sendFile(path.join(__dirname + '/views/index.html'));

});

router.get('/register', function (req, res, next) {
  res.sendFile(path.join(__dirname, '../views/signup.html'));
});
router.get('/main', function (req, res, next) {
  res.sendFile(path.join(__dirname, '../views/main.html'));
});
router.get('/create', function (req, res, next) {
  res.sendFile(path.join(__dirname, '../views/create.html'));
});

router.post('/detail', function (req, res, next) {
  res.render(path.join(__dirname, '../views/detail.ejs') ,{item_id: req.body.product_id});
  // res.send(req.body.product_id);
  console.log(req.body.product_id);
})

module.exports = router;