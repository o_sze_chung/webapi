var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use('/static', express.static('public'));

app.set('view engine', 'html');

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

app.use(express.static(__dirname + '/views'));


var routes = require('./routes/router');
app.use('/', routes);

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

// listen on port 3210


  app.listen(4000, function () {
    console.log('Listening on port 4000...');
  });


