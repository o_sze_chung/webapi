var hostname = "http://localhost:3000/"
$(document).ready(function () {
	$(".menu-icon").on("click", function () {
		$("nav ul").toggleClass("showing");
	});

	$("#logout").click(function () {
		$.ajax({
			url: hostname + "logout",
			type: "GET",
			dataType: "jsonp",
			success: function (data) {
				window.location.href = '/';
			}
		})
	})



	$('#create').click(function () {
		window.location.href = '/create';
	});


	$("#edit").click(function () {
		var product_id = $("#product_id").val()
		var Location = $("#Location").val()
		var Telephone = $("#Telephone").val()
		var Productdescription = $("#Productdescription").val()
		var Price = $("#Price").val()
		

		if (Price == "" ||  Productdescription == "" ||Location == "" || Telephone.length == ""){
			alert( "Product Description or Price or Location or Telephone is not null")
		}else if (Telephone.length === 8){
			var edit_data = {
				item_id: product_id,
				Productdescription: Productdescription,
				Price: Price,
				Location: Location,
				Telephone: Telephone
				
			}
			
			fetch("http://localhost:3000/update_item", {
				method: 'PUT',
				body: JSON.stringify(edit_data),
				headers: new Headers({
					'Content-Type': 'application/json'
				})
			})
		}else {
			alert("TeletePhone number length must 8")
		}
	})

	$("#delete_item").click(function () {
		var product_id = $("#product_id").val()
		var data = { item_id: product_id };
		fetch(hostname + "delete_item", {
			method: 'delete',
			body: JSON.stringify(data), 
			headers: new Headers({
				'Content-Type': 'application/json'
			})
		}).then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(response => window.location.href = "/main");
	})

	$("#add_comment").click(function (){
		var comment = $("#comment").val()
		var product_id = $("#product_id").val()
		var username = $("#CheckPermission").val()
		console.log(username)

		$.ajax({
			url: hostname + "comment",
			data: { 
				ProductID: product_id, 
				Comment: comment,
				Username: username
			},
			type: "POST",
			dataType: "application/jsonp",
			success: function (data) {
				location.reload();
			}
		});
		location.reload();
	})

})

$.ajax({
	url: hostname + "username",
	type: "GET",
	dataType: "jsonp",
	success: function (data) {
		if (data.name != "") {
			$("#username").text(data.name)
			$("#CheckPermission").val(data.name)

		} else {
			window.location.href = '/';
		}

	}
})


var product_id = $("#product_id").val()


$.ajax({
	url: hostname + "item/" + product_id,
	type: "GET",
	dataType: "jsonp",
	success: function (data) {
		$("#product-description").append(
			'<h1>' +
			data.ProductName +
			'</h1>' +
			'<p>' +
			data.Productdescription +
			'</p>'
		)
		$(".left-column").append(
			'<img class="active" src="http://localhost:3000/image/' + data.ProductImage + '" alt="">'
		)
		$(".detail").append(
			'<p>' +
			"Price: $" + data.Price +
			'</p>'+
			"Location: " + data.Location +
			'</p>' + 
			"Telephone: " + data.Telephone +
			'</p>' + 
			"Created by: " + data.Username +
			'</p>'

		)
		var check = $("#CheckPermission").val()
		if (check === data.Username) {
			$('#change').show()
		} else {
			$('#change').hide()
		}
		//console.log(data)

	}
})

$.ajax({
	url: hostname + "comment/" + product_id,
	type: "GET",
	dataType: "jsonp",
	success: function (data) {
		var check = $("#CheckPermission").val()
		for (i = 0; i < data.length; i++) {
		console.log(data[i])
		$("#all_comment").append(
			// data[i].Comment + '<br>'
			'<div class="commentbox">' +
            '<div class="commentbox_username">' +
			'<strong>' + data[i].Username + '</strong>' +
			( check == data[i].Username ?
			 '</div>': "" ) +
            data[i].Comment +
			'<form onsubmit="setTimeout(function () { window.location.reload(); }, 10)" method="post" action="http://localhost:3000/delete_comment/{{id}}?_method=DELETE">' +
				'<input type="hidden" name="_method" value="' + data[i]._id + '">' + 
				'<button type="submit" onClick="location.reload()" class="deletebtn" id="Delete_Comment' + i + '"' + '>' + 'Delete'  + '</button>' +
				'</form>' +
			'<h2 class="modify-title" id="exampleModalCenterTitle">Change The Comment</h2>' +
			'<form onsubmit="setTimeout(function () { window.location.reload(); }, 10)" method="POST" action="http://localhost:3000/edit_comment/{{id}}?_method=PUT">' +
			'<input type="hidden" name="selected_id" value=' + data[i]._id + '>' +
			'<input type="text" class="form-control" name="change_comment" required>'+
			'<br>'+
			'<button type="submit" class="commentchange">Save changes</button>' +
			'</form>'+
			'</div>'
		)

		}
		
	}
})
