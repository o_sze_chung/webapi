var item = require('../models/item');
var multer = require('multer');
const path = require('path')
const storage = multer.diskStorage({
    destination: function(req, file, cb){
      cb(null, './uploads/');
    },
    filename: function(req, file, cb){
      cb(null, file.originalname);
    }
  });
var upload = multer({storage: storage})

exports.ItemGet = function (req, res, next){
    item.find({}, function (err, item) {
        //console.log(item)
        res.jsonp(item)
      })
}

exports.ItemPost = upload.single('ProductImage') ,function (req, res, next){
  var image = req.file.filename
  //console.log(req.file)
  var CreateData = { 
    User: req.body.Username,
    Image: image,
    Name: req.body.ProductName,
    Description: req.body.Productdescription,
    Location: req.body.Location,
    Price: req.body.Price,
    PhoneNumber: req.body.Telephone
  }
  

}

exports.ItemGetOne = function (req, res ,next){
    item.findOne({ _id: req.params.item_id }, function (err, item) {
        res.jsonp(item)
      })
}

exports.ItemPut = function (req, res, next) {
    item.findByIdAndUpdate(req.body.item_id, {
        $set: {
          Productdescription: req.body.Productdescription,
          Price: req.body.Price,
          Location: req.body.Location,
          Telephone: req.body.Telephone
        }
      }, (err, result) => {
        if (err) return console.log(err)
        console.log(result)
      })
}

exports.ItemDelete = function (req, res, next) {
    item.findByIdAndDelete(req.body.item_id, (err, result) => {
        if (err) return res.send(err)
        res.send(result)
      })
}
