var Comment = require('../models/comment');

exports.CommentPost = function (req, res, next){
    var commentData = {
      ProductID: req.body.ProductID,
      Comment: req.body.Comment,
      User: req.body.Username
    }
    
  }

exports.CommentGet = function (req, res, next) {
    Comment.find({ProductID: req.params.ProductID} , function (err, item) {
      res.jsonp(item)
    })
  }

exports.CommentPut = function (req, res) {

    Comment.findByIdAndUpdate(req.body.selected_id, {
      $set: {
        Comment: req.body.change_comment
      }
    }, (err, result) => {
      if (err) return console.log(err)
      console.log(result)
    })
  }

exports.CommentDelete = function (req, res) {
    Comment.findByIdAndDelete(req.body._method, (err, result) => {
      if (err) return res.send(err)
      console.log(result)
    })
  }