var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Item = require('../models/item');
var Items = require('../models/item_model');
var Comment = require('../models/comment');
var Comments = require('../models/comment_model');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var multer = require('multer');
const path = require('path')
const storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null, './uploads/');
  },
  filename: function(req, file, cb){
    cb(null, file.originalname);
  }
});

const methodOverride = require('method-override');
router.use(methodOverride('_method'));

var upload = multer({storage: storage})


 var username = "";
router.use(bodyParser.urlencoded({ extended: true }));


router.post('/', function (req, res, next) {

  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Password doesn match!');
    err.status = 400;
    res.send('Password doesn match!');
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      passwordConf: req.body.passwordConf
    }

    User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
        username = user.username;
        return res.redirect('/username');
      }
    });

  } else if (req.body.logemail && req.body.logpassword) {
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password!');
        err.status = 401;
        return next(err);
      } else {
        req.session.userId = user._id;
        username = user.username;
         res.redirect('/username');

      }
    });
  } else {
    var err = new Error('All fields are required!');
    err.status = 400;
    return next(err);
  }
})

router.get("/username", function (req, res, next) {
  res.jsonp({"name":username});
})
router.get('/item', Items.ItemGet);




router.get('/main', function (req, res, next) {
  User.findById(req.session.userId)
    .exec(function (error, user) {

        if (user === null) {
          var err = new Error('');
          err.status = 400;
          res.send("Fail")
        } else {
          username = user.username;
          res.jsonp({"name":username});

      }
    });
});
  
router.post('/create', upload.single('ProductImage') ,function (req, res, next){
  var image = req.file.filename
  //console.log(req.file)
  var CreateData = {
    ProductImage: image,
    ProductName: req.body.ProductName,
    Productdescription: req.body.Productdescription,
    Location: req.body.Location,
    Price: req.body.Price,
    Telephone: req.body.Telephone,
    Username: req.body.Username
  }
  
  Item.create(CreateData, function (error, user) {
    if (error) {
      console.log(error)
    } else {

    }
  });
})

router.get('/logout', function (req, res, next) {
  if (req.session) {
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        username = "";
        return res.jsonp({"name":username});
      }
    });
  }
});

module.exports = router;