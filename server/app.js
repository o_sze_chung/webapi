var express = require('express');
var app = express();
var cors = require('cors')
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
mongoose.set('useFindAndModify', false);
app.set('view engine', 'html');
app.use('/static', express.static('public'));
mongoose.connect('mongodb+srv://xavier:Zz123456@website-kt96t.mongodb.net/test?retryWrites=true&w=majority', {


useUnifiedTopology: true,
useNewUrlParser: true,
})
.then(() => console.log('DB Connected!'))
.catch(err => {
console.log("DB cannnot connect!");
});

var db = mongoose.connection;

app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var routes = require('./routes/router');
app.use('/', routes);


app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

app.use(cors())
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

  app.listen(3000, function () {
    console.log('Listening on port 3000...');
  });


